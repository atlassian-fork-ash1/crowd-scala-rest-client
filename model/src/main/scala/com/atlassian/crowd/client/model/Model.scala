package com.atlassian.crowd.client.model

import monocle._, Monocle._
import monocle.function.Empty
import monocle.macros.{ GenIso, Lenses }
import scalaz.{ Either3, Equal, Left3, Middle3, Right3 }
import scalaz.std.string._
import scalaz.std.tuple._

@Lenses("_")
case class User(name: String,
  firstName: String,
  lastName: String,
  displayName: String,
  email: String,
  active: Boolean,
  key: Option[String],
  createdDate: Option[Long],
  updatedDate: Option[Long])
object User {
  implicit def equal: Equal[User] = Equal.equalA
  def _keyO = _key composePrism some
}

@Lenses("_")
case class UserWithAttributes(name: String,
  firstName: String,
  lastName: String,
  displayName: String,
  email: String,
  active: Boolean,
  key: Option[String],
  createdDate: Option[Long],
  updatedDate: Option[Long],
  attributes: Attributes)
object UserWithAttributes {
  implicit def equal: Equal[UserWithAttributes] = Equal.equalA
  def apply(user: User, attributes: Attributes): UserWithAttributes =
    _userAndAttributes.reverseGet((user, attributes))

  def _keyO = _key composePrism some
  def _user = _userAndAttributes composeLens first
  def _userAndAttributes = Iso[UserWithAttributes, (User, Attributes)](
    uwa => (User(uwa.name, uwa.firstName, uwa.lastName, uwa.displayName, uwa.email, uwa.active, uwa.key, uwa.createdDate, uwa.updatedDate), uwa.attributes)) {
      case (u, attributes) =>
        UserWithAttributes(u.name, u.firstName, u.lastName, u.displayName, u.email, u.active, u.key, u.createdDate, u.updatedDate, attributes)
    }
}

/**
 * A template for a [[User]] used to create new users.
 *
 * Note that Crowd versions < 2.9 will not set attributes on the created user.
 */
@Lenses("_")
case class UserTemplate(name: String,
  firstName: String,
  lastName: String,
  displayName: String,
  email: String,
  active: Boolean,
  passwordCredential: PasswordCredential,
  attributes: Attributes)

case class Attributes(attributes: Map[String, Set[String]])
object Attributes {
  implicit def equal: Equal[UserTemplate] = Equal.equalA
  def _attributes = GenIso[Attributes, Map[String, Set[String]]]
}

@Lenses("_")
case class Group(name: String,
  description: Option[String],
  active: Boolean)
object Group {
  implicit def equal: Equal[Group] = Equal.equalA
  def _descriptionO = _description composePrism some
}

sealed abstract case class SessionToken(token: String)
object SessionToken {
  def fromString(s: String): Option[SessionToken] =
    _token.getOption(s)
  implicit def equal: Equal[SessionToken] = Equal.equalA
  def _token: Prism[String, SessionToken] =
    Prism[String, SessionToken](s => if (s.isEmpty) None else Some(new SessionToken(s) {}))(_.token)
}

sealed trait ValidationFactorType
trait ValidationFactor {
  val name: String
  val value: String
  final def asTuple = (name, value)
}
object ValidationFactor {
  implicit def equal: Equal[ValidationFactor] = Equal.equalBy(_.asTuple)

  def _name = Getter[ValidationFactor, String](_.name)
  def _value = Getter[ValidationFactor, String](_.value)

  import ValidationFactors._
  private[this] def split: ValidationFactor => Either3[(String, String), String, String] = {
    case CustomValidationFactor("remote_address", value) => Either3.middle3(value)
    case RemoteAddress(value) => Either3.middle3(value)
    case CustomValidationFactor("X-Forwarded-For", value) => Either3.right3(value)
    case XForwardedFor(value) => Either3.right3(value)
    case CustomValidationFactor(name, value) => Either3.left3((name, value))
  }
  def _custom = Prism[ValidationFactor, (String, String)](vf => split(vf) match {
    case Left3(nv) => Some(nv)
    case _ => None
  })((CustomValidationFactor.apply _).tupled)
  def _remoteAddress = Prism[ValidationFactor, String](vf => split(vf) match {
    case Middle3(v) => Some(v)
    case _ => None
  })(RemoteAddress.apply)
  def _xForwardedFor = Prism[ValidationFactor, String](vf => split(vf) match {
    case Right3(v) => Some(v)
    case _ => None
  })(XForwardedFor.apply)
}

object ValidationFactors {
  case class CustomValidationFactor(name: String, value: String) extends ValidationFactor
  case class RemoteAddress(value: String) extends ValidationFactor { val name = "remote_address" }
  case class XForwardedFor(value: String) extends ValidationFactor { val name = "X-Forwarded-For" }
}

sealed trait UserAuthenticationContext
object UserAuthenticationContext {
  implicit def equal: Equal[PasswordCredential] = Equal.equalA

  def _user = Lens[UserAuthenticationContext, String] {
    case UserAuthenticationContextWithPassword(user, _, _) => user
    case UserAuthenticationContextWithoutPassword(user, _) => user
  }(user => {
    case u @ UserAuthenticationContextWithPassword(_, _, _) => u.copy(username = user)
    case u @ UserAuthenticationContextWithoutPassword(_, _) => u.copy(username = user)
  })
  def _password = _withPassword composeLens second
  def _validationFactors = Lens[UserAuthenticationContext, Seq[ValidationFactor]] {
    case UserAuthenticationContextWithPassword(_, _, vfs) => vfs
    case UserAuthenticationContextWithoutPassword(_, vfs) => vfs
  }(vfs => {
    case u @ UserAuthenticationContextWithPassword(_, _, _) => u.copy(validationFactors = vfs)
    case u @ UserAuthenticationContextWithoutPassword(_, _) => u.copy(validationFactors = vfs)
  })

  def _withPassword: Prism[UserAuthenticationContext, (String, PlainTextCredential, Seq[ValidationFactor])] =
    Prism[UserAuthenticationContext, (String, PlainTextCredential, Seq[ValidationFactor])] {
      case UserAuthenticationContextWithPassword(user, pass, vfs) => Some((user, pass, vfs))
      case _ => None
    }((UserAuthenticationContextWithPassword.apply _).tupled)
  def _withoutPassword: Prism[UserAuthenticationContext, (String, Seq[ValidationFactor])] =
    Prism[UserAuthenticationContext, (String, Seq[ValidationFactor])] {
      case UserAuthenticationContextWithoutPassword(user, vfs) => Some((user, vfs))
      case _ => None
    }((UserAuthenticationContextWithoutPassword.apply _).tupled)
}

case class UserAuthenticationContextWithPassword(username: String,
  passwordCredential: PlainTextCredential,
  validationFactors: Seq[ValidationFactor] = Seq()) extends UserAuthenticationContext

case class UserAuthenticationContextWithoutPassword(username: String,
  validationFactors: Seq[ValidationFactor] = Seq()) extends UserAuthenticationContext

sealed trait PasswordCredential
object PasswordCredential { self =>
  implicit def equal: Equal[PasswordCredential] = Equal.equalA

  implicit object empty extends Empty[PasswordCredential] {
    def empty = self._none
  }

  def _none = Prism[PasswordCredential, Unit] {
    case EncryptedCredential("X") => Some(())
    case _ => None
  }(_ => EncryptedCredential("X"))

  def _encrypted = Prism[PasswordCredential, String] {
    case EncryptedCredential(ec) if ec != "X" => Some(ec)
    case _ => None
  }(ec => EncryptedCredential(ec))

  def _plainText = Prism[PasswordCredential, String] {
    case PlainTextCredential(pt) => Some(pt)
    case _ => None
  }(pt => PlainTextCredential(pt))
}
case class EncryptedCredential(credential: String) extends PasswordCredential {
  override def toString =
    s"EncryptedCredential(xxxxxxxx)"
}
case class PlainTextCredential(credential: String) extends PasswordCredential {
  override def toString =
    s"PlainTextCredential(xxxxxxxx)"
}

@Lenses("_")
case class CookieConfiguration(domain: String, secure: Boolean, name: String)
