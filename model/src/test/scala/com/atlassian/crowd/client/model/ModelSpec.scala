package com.atlassian.crowd.client.model

import ModelGenerators._
import ValidationFactors._
import org.scalacheck.{ Arbitrary, Gen }
import Arbitrary.arbitrary
import monocle.law.discipline.PrismTests
import org.specs2.ScalaCheck
import org.specs2.mutable.Specification

import scalaz._
import Scalaz._

object ModelGenerators {
  implicit val arbPasswordCredential: Arbitrary[PasswordCredential] = Arbitrary {
    Gen.oneOf(
      Gen.const(EncryptedCredential("X")),
      arbitrary[String].filter(_ != "X").map(EncryptedCredential.apply),
      arbitrary[String].map(PlainTextCredential.apply))
  }

  implicit val arbValidationFactor: Arbitrary[ValidationFactor] = Arbitrary {
    val genRemoteAddress = Gen.oneOf(
      arbitrary[String].map(RemoteAddress.apply),
      arbitrary[String].map(CustomValidationFactor("remote_address", _)))
    val genXForwardedFor = Gen.oneOf(
      arbitrary[String].map(XForwardedFor.apply),
      arbitrary[String].map(CustomValidationFactor("X-Forwarded-For", _)))
    val genCustom = for {
      name <- arbitrary[String]
      value <- arbitrary[String]
    } yield CustomValidationFactor(name, value)

    Gen.oneOf(genRemoteAddress, genXForwardedFor, genCustom)
  }
}

object ModelSpec extends Specification with ScalaCheck {
  "password credential optics" should {
    "_none" should {
      "obey the prism laws" in PrismTests(PasswordCredential._none).all
    }
    "_encrypted" should {
      "obey the prism laws" in PrismTests(PasswordCredential._encrypted).all
    }
    "_plainText" should {
      "obey the prism laws" in PrismTests(PasswordCredential._plainText).all
    }
  }

  "validation factor optics" should {
    "_remoteAddress" should {
      "obey the prism laws" in PrismTests(ValidationFactor._remoteAddress).all
      "handle 'custom' remote_address validation factors" in prop { value: String =>
        val vf = CustomValidationFactor("remote_address", value)
        ValidationFactor._remoteAddress.getOption(vf) must beSome(value)
      }
    }
    "_xForwardedFor" should {
      "obey the prism laws" in PrismTests(ValidationFactor._xForwardedFor).all
      "handle 'custom' X-Forwarded-For validation factors" in prop { value: String =>
        val vf = CustomValidationFactor("X-Forwarded-For", value)
        ValidationFactor._xForwardedFor.getOption(vf) must beSome(value)
      }
    }
    "_custom" should {
      "obey the prism laws" in PrismTests(ValidationFactor._custom).all
    }
  }
}
