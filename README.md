## Crowd Scala REST client

An asynchronous [Crowd user management](https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+Resources#CrowdRESTResources-UserAuthenticationResource) client.

[Builds](https://server-gdn-bamboo.internal.atlassian.com/browse/CWDCLIENT-SCALACLIENT)
